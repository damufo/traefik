# Traefik

Creación da rede web non caso de non existir:

```
$ docker network create web
```

Ficheiros extra os cales se crean no cartafol onde está o docker-compose.yml


Ficheiro .env

```
DOMAIN_SERVER_BASE=damufo.com
DOMAIN_SERVER_BASE2=damufo.eu 
EMAIL=dani@damufo.eu
```

Creación do ficheiro acme.json:
```
$ touch acme.json
$ chmod 600 acme.json
```
Neste ficheiro non se pon nada, será usado e enchido polo contedor traefik.

Arrancar o servizo

```
$ docker-compose up -d traefik
```

Comprobación de posibles erros

```
$ docker-compose logs -f
```

